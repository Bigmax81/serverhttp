TP2 : Serveur HTTP
Emmanuel Garreau et Laetitia Getti

Pour lancer le serveur, exécuter : 

javac src/server/WebServer.java
java src/server/WebServer.java

Le serveur se lance sur l'adresse localhost (127.0.0.1) et sur le port 3001.