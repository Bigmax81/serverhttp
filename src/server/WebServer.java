///A Simple Web Server (WebServer.java)

package server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;

/**
 * Example program from Chapter 1 Programming Spiders, Bots and Aggregators in
 * Java Copyright 2001 by Jeff Heaton
 *
 * WebServer is a very simple web-server. Any request is responded with a very
 * simple web-page.
 *
 * @version 1.0
 */
public class WebServer {

    /**
     * WebServer constructor.
     */
    protected void start() {
        ServerSocket s;

        System.out.println("Webserver starting up on port 3001");
        System.out.println("(press ctrl-c to exit)");
        try {
            // create the main server socket
            s = new ServerSocket(3001);
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return;
        }

        System.out.println("Waiting for connection\r\n");
        for (;;) {
            try {
                // wait for a connection
                Socket remote = s.accept();
                // remote is now the connected socket

                BufferedInputStream in = new BufferedInputStream(remote.getInputStream());
                PrintWriter out = new PrintWriter(remote.getOutputStream());

                /*
                String str = ".";
                String request = "";
                while (str != null && !str.equals("")) {
                    str = in.readLine();
                    if (str != null){
                        request += str+"\r\n";
                    }
                }*/

                String request = "";
                int act = '\0';
                int precedent = '\0';
                boolean ligne = false;
                while((act = in.read()) != -1 && !(ligne && precedent == '\r' && act == '\n')) {
                    if(precedent == '\r' && act == '\n') {
                        ligne = true;
                    } else if(!(precedent == '\n' && act == '\r')) {
                        ligne = false;
                    }
                    precedent = act;
                    request += (char) act;
                }

                String[] split;
                String method;
                String route;
                if (!request.equals("")) {
                    // System.out.println("Connection with client : \r\n");
                    // System.out.println(request);

                    split = request.split(" ");
                    method = split[0];
                    route = split[1];

                    if (!route.equals("/favicon.ico")){
                        switch (method) {
                            case "GET" : {
                                System.out.println("Méthode : GET");
                                System.out.println("Route : " + route + "\n");
                                httpGet(out, route);
                                break;
                            }
                            case "PUT" : {
                                System.out.println("Méthode : PUT");
                                System.out.println("Route : " + route + "\n");
                                httpPut(in, out, route);
                                break;
                            }
                            case "POST" : {
                                System.out.println("Méthode : POST");
                                System.out.println("Route : " + route + "\n");
                                httpPost(in, out, route);
                                break;
                            }
                            case "DELETE" : {
                                System.out.println("Méthode : DELETE");
                                System.out.println("Route : " + route + "\n");
                                httpDelete(out, route);
                                break;
                            }
                            default : {
                                System.out.println("Méthode non gérée\n");
                            }
                        }
                    }
                }

                // Send the response
                // Send the headers

                remote.close();
            } catch (Exception e) {
                System.out.println("Error: " + e);
            }
        }
    }
    /**
     * Méthode qui renvoie une réponse à une requête GET en fonction de l'URL demandé.
     * @param route : url de la page demandée
     * @param out : Flux de sortie sur lequel envoyer la réponse
     */
    protected static void httpGet(PrintWriter out, String route){
        // Redirection pour la page racine
        if(route.equals("/")){
            route = "/index.html";
        }
        String routeFile = "src/resources" + route;
        File ressource = new File(routeFile);
        String mimesType = "";
        try {
            mimesType = Files.probeContentType(ressource.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(ressource.exists() && ressource.isFile()) {
            send200Header(out, mimesType);
            displayHTMLPage(new File(routeFile), out);
            out.flush();
        } else {
            send404(out);
            out.flush();
        }
    }

    /**
     * PUT method
     *
     * @param in Input for the body
     * @param out Output we want to print the page on.
     * @param route Route of the file
     */
    protected static void httpPut(BufferedInputStream in, PrintWriter out, String route) {
        String routeFile = "src/resources"+route;
        File ressource = new File(routeFile);
        String mimesType = "";
        try {
            mimesType = Files.probeContentType(ressource.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean test;
        if(ressource.exists()) {
            ressource.delete();
            try {
                ressource.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            test = false;
        } else{
            test = true;
        }

        try {
            BufferedOutputStream fileOut = new BufferedOutputStream(new FileOutputStream(ressource));
            byte[] buffer = new byte[256];
            while(in.available() > 0) {
                int nbRead = in.read(buffer);
                fileOut.write(buffer, 0, nbRead);
            }
            fileOut.flush();
            fileOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (test == false){
            send200Header(out, mimesType);
            out.flush();
        } else{
            send201Header(out, route, mimesType);
        }
        displayHTMLPage(new File(routeFile), out);
        out.flush();
    }

    /**
     * POST method
     *
     * @param in Input for the body
     * @param out Output we want to print the page on.
     * @param route Route of the file
     */
    protected static void httpPost(BufferedInputStream in, PrintWriter out, String route){
        String routeFile = "src/resources"+route;
        File ressource = new File(routeFile);
        String mimesType = "";
        try {
            mimesType = Files.probeContentType(ressource.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean existe = ressource.exists();
        if (existe){
            send200Header(out, mimesType);
        }
        else{
            send201Header(out, route, mimesType);
        }
        try{
            BufferedOutputStream fileOut = new BufferedOutputStream(new FileOutputStream(ressource, existe));
            byte[] buffer = new byte[256];
            while(in.available() > 0) {
                int nbRead = in.read(buffer);
                fileOut.write(buffer, 0, nbRead);
            }
            fileOut.flush();
            fileOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        displayHTMLPage(new File(routeFile), out);
        out.flush();
    }

    /**
     * DELETE method
     *
     * @param out Output we want to print the page on.
     * @param route Route of the file
     */
    protected static void httpDelete(PrintWriter out, String route){
        String routeFile = "src/resources"+route;
        File ressource = new File(routeFile);
        String mimesType = "";
        try {
            mimesType = Files.probeContentType(ressource.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (ressource.exists()){
            ressource.delete();
            send200Header(out, mimesType);
            out.flush();
        } else{
            send404(out);
            out.flush();
        }
    }

    /**
     * Sends on the output a HTTP 200 header.
     *
     * @param out Output we want to print the header on.
     */
    protected static void send200Header(PrintWriter out, String contentType) {
        out.println("HTTP/1.0 200 OK");
        out.println("Content-Type: "+contentType);
        out.println("");
    }

    /**
     * Sends on the output a HTTP 201 header.
     *
     * @param out Output we want to print the header on.
     */
    protected static void send201Header(PrintWriter out, String route, String contentType) {
        out.println("HTTP/1.0 201 Created");
        out.println("Content-Type: "+contentType);
        out.println("Content-Location: "+route);
        out.println("");
    }

    /**
     * Sends on the output a HTTP 404 page.
     *
     * @param out Output we want to print the page on.
     */
    protected static void send404(PrintWriter out) {
        out.println("HTTP/1.0 404 Not Found");
        out.println("Content-Type: text/html;charset=UTF-8");
        out.println("");
        displayHTMLPage(new File("src/resources/404.html"), out);
    }


    /**
     * Sends on the output a HTTP 500 page.
     *
     * @param out Output we want to print the page on.
     */
    protected static void send500(PrintWriter out) {
        out.println("HTTP/1.0 500 Internal Server Error");
        out.println("Content-Type: text/html;charset=UTF-8");
        out.println("Server: Bot");// this blank line signals the end of the headers
        out.println("");
        displayHTMLPage(new File("src/resources/500.html"), out);
    }

    /**
     * Displays on the "out" the HTML File given in parameters.
     *
     * @param htmlFileName HTML Page we want to display.
     * @param out Output we want to print the page on.
     */
    protected static void displayHTMLPage(File htmlFileName, PrintWriter out) {
        try {
            FileInputStream inFile = new FileInputStream(htmlFileName);
            int fileLength = (int) htmlFileName.length();
            byte Bytes[] = new byte[fileLength]; // file size = inFile.read(Bytes))
            inFile.read(Bytes);
            String file1 = new String(Bytes); // file content
            out.println(file1);
            // close file
            inFile.close();
        } catch (Exception e) {
            System.err.println("Erreur dans displayHTMLPage " + htmlFileName + e);
        }
    }

    /**
     * Start the application.
     *
     * @param args Command line parameters are not used.
     */
    public static void main(String[] args) {
        WebServer ws = new WebServer();
        ws.start();
    }


}
